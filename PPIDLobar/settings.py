import sys

from pathlib import Path
if 'runserver' in sys.argv or 'makemigrations' in sys.argv or 'migrate' in sys.argv:
# if 'runserver' in sys.argv:
    from .config_local import *
else:
    from .config_server import *

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

sys.dont_write_bytecode = True


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%iy=ndha+-vol&-a00@r*dro(*^v1j_ziax!*dfk=hom8)ori3'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']
CSRF_TRUSTED_ORIGINS = ['https://ppid.lombokbaratkab.go.id']


#CSRF_TRUSTED_ORIGINS = ['https://*.lombokbaratkab.go.id','http://10.91.47.26/']

# Application definition

INSTALLED_APPS = [
    'admin_interface', 
    'colorfield', 
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'ppid',
    'accounts',
    'rest_framework',
    'corsheaders',
    'hitcount',
    'ckeditor',
    'ckeditor_uploader',
    'crispy_forms',
    'captcha',
]
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'
CAPTCHA_LENGTH = '5'
CAPTCHA_NOISE_FUNCTIONS = ('captcha.helpers.noise_null',)
CAPTCHA_BACKGROUND_COLOR = '#F5EFE6'
CAPTCHA_FOREGROUND_COLOR = '#3C2317'


CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_BASEPATH = '/static/ckeditor/ckeditor/'
CKEDITOR_IMAGE_BACKEND = "pillow"


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'PPIDLobar.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR/'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'PPIDLobar.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

#FROM SERVER
# DATABASES = {
#      'default': {
#          'ENGINE'    : 'django.db.backends.mysql', 
#          'NAME'      : 'db_ppidlobar', 
#          'USER'      : 'root', 
#          'PASSWORD'  : '10121997', 
#          'HOST'      : '127.0.0.1', 
#          'PORT'      : '3306'
#      }
#  }

#FROM LOCAL
DATABASES = {
   'default': {
       'ENGINE'    : 'django.db.backends.mysql', 
       'NAME'      : DB_NAME, 
       'USER'      : DB_USER, 
       'PASSWORD'  : DB_PASSWORD, 
       'HOST'      : '127.0.0.1', 
       'PORT'      : '3306'
   }
}

# CACHES = {
#     "default": {
#         "BACKEND": "django_redis.cache.RedisCache",
#         "LOCATION": "redis://127.0.0.1:6379/0",
#         "OPTIONS": {
#             "CLIENT_CLASS": "django_redis.client.DefaultClient",
#         }
#     }
# }


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
#    {
#        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'id'

TIME_ZONE = 'Asia/Makassar'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR/ 'staticfiles'
STATICFILES_DIRS = [
    BASE_DIR / 'static',
]

CRISPY_TEMPLATE_PACK = 'bootstrap4'

SITE_ID = 1
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES':('rest_framework.renderers.JSONRenderer',)
}

CKEDITOR_CONFIGS = {'default': {'width': '100%', 'toolbar_Basic': [[
    'Source', '-', 'Bold', 'Italic']], 'toolbar_YourCustomToolbarConfig': [
    {'name': 'document', 'items': ['Source', '-', 'Save', 'NewPage',
    'Preview', 'Print', '-', 'Templates']}, {'name': 'clipboard', 'items':
    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
    'Redo']}, {'name': 'editing', 'items': ['Find', 'Replace', '-',
    'SelectAll']}, {'name': 'basicstyles', 'items': ['Bold', 'Italic',
    'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']
    }, '/', {'name': 'paragraph', 'items': ['NumberedList', 'BulletedList',
    '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-',
    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-',
    'BidiLtr', 'BidiRtl', 'Language']}, {'name': 'links', 'items': ['Link',
    'Unlink', 'Anchor']}, {'name': 'insert', 'items': ['Image', 'Flash',
    'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak',
    'Iframe']}, '/', {'name': 'styles', 'items': ['Styles', 'Format',
    'Font', 'FontSize']}, {'name': 'colors', 'items': ['TextColor',
    'BGColor']}, {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']}, {
    'name': 'about', 'items': ['About', 'CodeSnippet']}], 'toolbar':
    'YourCustomToolbarConfig', 'toolbarCanCollapse': True, 'tabSpaces': 4,
    'extraPlugins': ','.join(['uploadimage', 'div', 'autolink', 'autoembed',
    'embedsemantic', 'widget', 'lineutils', 'clipboard', 'dialog',
    'dialogui', 'elementspath', 'codesnippet'])}}

MEDIA_URL = '/fileppid/'
MEDIA_ROOT = BASE_DIR / 'fileppid'


