from django.contrib import admin
from .models import *
from django.contrib.contenttypes.models import ContentType


# Register your models here.

admin.site.register(Dinas)
admin.site.register(Type_data)

class MyModelData(admin.ModelAdmin):
    list_display = ['title', 'dinas', 'date']
    fields = ['title', 'dinas']

class MyModelMenu(admin.ModelAdmin):
    list_display = ['__str__', 'slug']

admin.site.register(Data, MyModelData)
admin.site.register(IpModel)
admin.site.register(Form_information)
admin.site.register(Type_pemohon)
admin.site.register(Type_action)
admin.site.register(sengketa)
admin.site.register(Type_layanan)
admin.site.register(layanan)
admin.site.register(slideshow)
admin.site.register(profilppid)
admin.site.register(tugasfungsi)
admin.site.register(visimisippid)
admin.site.register(strukturorganisasi)
admin.site.register(maklumatpelayanan)
admin.site.register(contact_person)
admin.site.register(halaman_statis)
admin.site.register(link_terkait)
admin.site.register(ContentType)
admin.site.register(Download_data)

# tambahan tabel menu
admin.site.register(menu, MyModelMenu)