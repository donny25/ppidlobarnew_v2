from django.db.models import fields
from django.db.models.query import QuerySet
from ppid.models import Data, Dinas, Form_information
from ppid.serializers import DataSerializers
from rest_framework  import viewsets, serializers
from django.http import HttpResponse, JsonResponse


class DataViewset(viewsets.ModelViewSet):
    queryset = Data.objects.all()[:100]
    serializer_class = DataSerializers
    http_method_names = ['get']
    

class DataFrontSerializer(serializers.ModelSerializer):
    dinas = serializers.CharField(source='dinas_')
    class Meta:
        model = Data
        fields = '__all__'
        
class PermohonanSerializer(serializers.ModelSerializer):
    action = serializers.CharField(source='action_')
    date = serializers.CharField(source='date_')

    class Meta: 
        model = Form_information
        fields = '__all__'

    
    # def get_queryset(self):
    #     queryset = Data.objects.all().values('id','dinas__title', 'title', 'responsible')[:3]
    #     return queryset 
    
    # def head(self, *args, **kwargs):
        # queryset = Data.objects.all().values('dinas','dinas__title')[:3]
        # query = list(queryset)

        # return JsonResponse(query, safe=False)

    
    # berita.photo.through.objects.filter(berita__site=siteID,
    #     berita__status=Status.PUBLISHED).values('berita__id',
    #     'berita__site__name', 'berita__kategori__nama', 'berita__judul',
    #     'berita__judul_seo', 'berita__isi_berita',
    #     'berita__admin__username', 'berita__created_at')

# def DataViewset(request):

#     query = Data.objects.all().values('id','dinas__title', 'title', 'responsible')[:5]
#     query1 = list(query)

#     return JsonResponse(query1, safe=False)

    
