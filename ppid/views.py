from django.http import request
from django.shortcuts import render, redirect, HttpResponse, get_object_or_404
from django.template import context
from django.template.context import Context
from django.db.models import Q

from ppid.viewset_api import DataFrontSerializer, PermohonanSerializer
from .models import *
from .forms import *
from django.contrib.sites.models import Site

from django.core.paginator import EmptyPage, Paginator
from django.contrib.contenttypes.models import ContentType

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView

#untuk membuat query
from django.db.models import Count, OuterRef, Subquery, Sum
import humanize
from . import menus

from hitcount.models import HitCount
from hitcount.views import HitCountMixin
from hitcount.models import Hit

from django.contrib import messages

# from hitcount.views import HitCountDetailView

# --------------------------------------------- GET DATA ----------------------------------------------------------
def get_siteID(request):
    siteID = Site.objects.filter(domain=request.get_host()).values_list('id',
        flat=True)
    if siteID.count() == 0:
        return HttpResponse(
            "domain '%s' belum terdaftar, silahkan daftar di halaman <a href='%s'>admin</a>"
             % (request.get_host(), '/admin'))
    siteID = siteID[0]
    return siteID

def get_hitCounter(request, obj, content_type):
    if content_type != 'site':
        hit_count = HitCount.objects.get_for_object(obj)
        hit_count_response = HitCountMixin.hit_count(request, hit_count)
        content_type_id = ContentType.objects.filter(model=content_type).first(
            )
        if content_type_id:
            hit_update = HitCount.objects.filter(object_pk=obj.id,
                content_type_id=content_type_id.id).values_list('hits',
                flat=True)
            if hit_update.count() > 0:
                obj.view_count = hit_update[0]
                obj.save()

def get_downloadCounter(request, obj, content_type):
    if content_type != 'site':
        hit_count = HitCount.objects.get_for_object(obj)
        hit_count_response = HitCountMixin.hit_count(request, hit_count)
        content_type_id = ContentType.objects.filter(model=content_type).first( 
            )
        if content_type_id:
            hit_update = HitCount.objects.filter(object_pk=obj.id,
                content_type_id=content_type_id.id).values_list('hits',
                flat=True)
            if hit_update.count() > 0:
                obj.download_count = hit_update[0]
                obj.save()

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

# --------------------------------------------- END GET DATA ----------------------------------------------------------

# --------------------------------------------- READ & DOWNLOAD ----------------------------------------------------------
                
def Detaildata(request, slug):
    siteID = get_siteID(request)
    siteUrl = Site.objects.filter(domain=request.get_host()).values_list('domain',
        flat=True)
    siteUrl = siteUrl[0]
    
    print("domain name")
    print(siteUrl)

    myMenu = menus.ClsMenus(siteID, False)
    datas = Data.objects.filter(site_id = siteID)

     #Filter Data Berdasar dinas
    # dataopd = Data.objects.filter(dinas_id = pk, site_id = siteID )
    # for i in dataopd:
    #     i.size = humanize.naturalsize(i.size)
    
    data = get_object_or_404(datas, slug=slug)
    get_hitCounter(request, data, 'data')
    data.size = humanize.naturalsize(data.size)

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    dip_show = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    context = {
        'data':data,
        'siteUrl':siteUrl,
        'opd_count':opd_count,
        'dip_show':dip_show,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
        # 'viewdis':viewdis,
    }
    return render(request, 'ppid/detail-data.html', context)

def Downloaddata(request, slug):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)
    datas = Data.objects.filter(site_id = siteID)
    
    data = get_object_or_404(datas, slug=slug)
    get_downloadCounter(request, data, 'data')
    data.size = humanize.naturalsize(data.size)

    context = {
        'data':data,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
        # 'viewdis':viewdis,
    }
    return render(request, 'ppid/download-data.html', context)

# --------------------------------------------- END READ & DOWNLOAD ----------------------------------------------------------

# --------------------------------------------- API SERIALIZER ----------------------------------------------------------
class DataApiView(ListAPIView):
    serializer_class = DataFrontSerializer
    queryset = Data.objects.all()
    
# class DataListApiView1(APIView):
#     def get(self, request, *args, **kwargs):
        
#         # Dapatkan Company ID dari tabel User Detail

#         datas = Data.objects.filter(site_id = get_siteID(request))[:20]      
        
#         serializer = DataSerializers(datas, many=True)
#         return Response(serializer.data, status=status.HTTP_200_OK)

class PermohonanListApiView(APIView):
    def get(self, request, *args, **kwargs):
        
        # Dapatkan Company ID dari tabel User Detail
        permohonan = Form_information.objects.filter(site_id = get_siteID(request)).order_by("-id")
        
        serializer = PermohonanSerializer(permohonan, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

# --------------------------------------------- END API SERIALIZER ----------------------------------------------------------

# --------------------------------------------- INDEX ----------------------------------------------------------
def index(request):
        #mengambil fungsi site
        siteID = get_siteID(request)
        
        #Menu
        myMenu = menus.ClsMenus(siteID, False)

        #Jumlah View dan Download
        view = Data.objects.filter(site_id=siteID).order_by('-view_count')[:7]
        download = Data.objects.filter(site_id=siteID).order_by('-download_count')[:7]

        #Mengambil Data Site
        site = Site.objects.filter(id = siteID)

        #Filter Site
        site1 = Site.objects.filter(id = '1')
        site2 = Site.objects.filter(id = '2')
        site3 = Site.objects.filter(id = '3')
        site4 = Site.objects.filter(id = '4')
        site5 = Site.objects.filter(id = '5')
        
        show = Data.objects.filter(site_id = siteID).order_by("-date")[:7]

        showsekolah = Data.objects.filter(site_id = '2').order_by("-id")[:20]
        showpuskesmas = Data.objects.filter(site_id = '3').order_by("-id")[:20]
        showdesa = Data.objects.filter(site_id = '4').order_by("-id")[:20]
        showkecamatan = Data.objects.filter(site_id = '5').order_by("-id")[:20]

        #Count Data 
        dip_count = Data.objects.filter(site_id = siteID).count()
        dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
        dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
        dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

        #Link Terkait
        link = link_terkait.objects.filter(site_id = siteID)

        #slider
        slide = slideshow.objects.filter(site_id = siteID)

        context = {
            # 'data':data,
            'slide':slide,
            'link':link,
            'site':site,
            'site1':site1,
            'site2':site2,
            'site3':site3,
            'site4':site4,
            'site5':site5,
            'show':show,
            'showkecamatan':showkecamatan,
            'showsekolah':showsekolah,
            'showdesa':showdesa,
            'showpuskesmas':showpuskesmas,
            'dip_count':dip_count,
            'dip3_count':dip3_count,
            'dip2_count':dip2_count,
            'dip1_count':dip1_count,
            'view':view,
            'download':download,
            'menu': myMenu.get_menus(), 
            'activeMenuList': myMenu.find_activeMenuList('Beranda')
            # 'viewdis':viewdis,
        }
        
        return render(request, 'ppid/index.html', context)

def search(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)


    site = Site.objects.filter(id = siteID)
    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
 
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)
    
    if request.method == "POST":
        searched = request.POST['searched']
        title = request.POST['title']

        datas = Data.objects.filter(type_data_id=searched, site_id = siteID, title__icontains=title)

        return render(request, 'ppid/search.html',{'searched':searched, 'datas':datas,'dip':dip,
        'site':site,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
        })
    else:
        return render(request, 'ppid/search.html')

def searchall(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)


    site = Site.objects.filter(id = siteID)
    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
 
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)
    
    if request.method == "POST":
        title = request.POST['title']

        datas = Data.objects.filter(site_id = siteID, title__icontains=title)

        return render(request, 'ppid/search.html',{'datas':datas,'dip':dip,
        'site':site,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')})
    else:
        return render(request, 'ppid/search.html')

def jumlah_permohonan_layanan(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)
    slide = slideshow.objects.filter(site_id = siteID)

    context = {
    'slide':slide,
    'menu': myMenu.get_menus(), 
    'activeMenuList': myMenu.find_activeMenuList('')
    }
    

    return render(request, 'ppid/laporan.html', context)

# --------------------------------------------- END INDEX ----------------------------------------------------------

# --------------------------------------------- DATA/DIP ----------------------------------------------------------
def DIP(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    dip = Data.objects.filter(site_id = siteID)

    dip1 = Data.objects.filter(site_id = siteID)
    dip_count = dip1.count()

    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    context = {
        'site':site,
        'dip':dip,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd':opd,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/dip.html', context)

def dip_opd(request, pk):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)
    
    #Filter Data Berdasar dinas
    dataopd = Data.objects.filter(dinas_id = pk, site_id = siteID )
    for i in dataopd:
        i.size = humanize.naturalsize(i.size)

    #Count data
    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    dip_show = Dinas.objects.filter(id = pk, site_id = siteID).annotate(jumlah=subQry)

    context = {
        'site':site,
        'dataopd':dataopd,
        'dip':dip,
        'dip_show':dip_show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/dip_opd.html', context)

def dip_read(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    dip = Data.objects.filter(site_id = siteID).order_by("-view_count")
    for i in dip:
        i.size = humanize.naturalsize(i.size)
    
    dip1 = Data.objects.filter(site_id = siteID)
    dip_count = dip1.count()

    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)


    context = {
        'site':site,
        'dip':dip,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        # 'opd':opd,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/dip_read.html', context)

def dip_download(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    dip = Data.objects.filter(site_id = siteID).order_by("-download_count")
    for i in dip:
        i.size = humanize.naturalsize(i.size)
    
    dip1 = Data.objects.filter(site_id = siteID)
    dip_count = dip1.count()

    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)


    context = {
        'site':site,
        'dip':dip,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        # 'opd':opd,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/dip_download.html', context)


# --------------------------------------------- END DATA/DIP ----------------------------------------------------------

# --------------------------------------------- HALAMAN STATIS ----------------------------------------------------------

def halaman(request, slug):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    # menulink = menu.objects.all()

    # print("mymenu")
    # print(menulink)
    Mn = menu.objects.filter(slug = slug).get()

    halaman = halaman_statis.objects.filter(site_id = siteID, menu_id=Mn.id)

    dip1 = Data.objects.filter(site_id = siteID)
    dip_count = dip1.count()
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)


    context = {
        'site':site,
        'halaman':halaman,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/halaman_statis.html', context)

# --------------------------------------------- END HALAMAN STATIS ----------------------------------------------------------

# --------------------------------------------- LAPORAN LAYANAN ----------------------------------------------------------

def layanan_permohonan(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)
    show = layanan.objects.filter(site_id = siteID, type_layanan = '1')

    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
 
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    context = {
        'site':site,
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/layanan_permohonan.html', context)

def layanan_waktu(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)
    show = layanan.objects.filter(site_id = siteID, type_layanan = '2')

    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
 
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    context = {
        'site':site,
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/layanan_waktu.html', context)

def layanan_diterima(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    show = layanan.objects.filter(site_id = siteID, type_layanan = '3')

    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
 
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    context = {
        'site':site,
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/layanan_diterima.html', context)

def layanan_ditolak(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    show = layanan.objects.filter(site_id = siteID, type_layanan = '4')

    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
 
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    context = {
        'site':site,
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    return render(request, 'ppid/layanan_ditolak.html', context)


# --------------------------------------------- END LAPORAN LAYANAN ----------------------------------------------------------


# --------------------------------------------- CONTACT -------------------------------------------------------------

def contact(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    contact = contact_person.objects.filter(site_id = siteID)
    
    dip1 = Data.objects.filter(site_id = siteID)
    dip_count = dip1.count()
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)


    context = {
        'site':site,
        'contact':contact,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('contact')
    }
    return render(request, 'ppid/contact.html', context)

# --------------------------------------------- END CONTACT ----------------------------------------------------------

# --------------------------------------------- FORM PERMINTAAN DATA ----------------------------------------------------------
def form_request(request):
    siteID = get_siteID(request)
    site = Site.objects.filter(id = siteID)
    myMenu = menus.ClsMenus(siteID, False)

    show = Form_information.objects.filter(site_id = siteID)

     #Count data
    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
 
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    form = RequestForm()
    if request.method == 'POST':
        form = RequestForm(request.POST, request.FILES)
        if form.is_valid():
            human = True
            post = form.save(commit=False)
            post.site_id = siteID
            post.save()
            return redirect('sukses')
        else:
            messages.error(request, 'Captcha Salah!')

    context = {
        'site':site,
        'form':form, 
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('')
    }
    # return render(request, 'ppid/form_request.html', context)
    return render(request, 'ppid/permohonan.html', context)

# --------------------------------------------- FORM PERMINTAAN DATA ----------------------------------------------------------

# --------------------------------------------- MENU PROFIL FRONTEND ----------------------------------------------------------
def profileppid(request):
    siteID = get_siteID(request)

    site = Site.objects.filter(id = siteID)
    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()

    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    profil = profilppid.objects.filter(site_id = siteID)
    tugas = tugasfungsi.objects.filter(site_id = siteID)
    organisasi = strukturorganisasi.objects.filter(site_id = siteID)
    misivisi = visimisippid.objects.filter(site_id = siteID)
    maklumat = maklumatpelayanan.objects.filter(site_id = siteID)

    myMenu = menus.ClsMenus(siteID, False)


    context = {
        'site':site,
        'profil':profil,
        'misivisi':misivisi,
        'maklumat':maklumat,
        'organisasi':organisasi,
        'tugas':tugas,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('Profil')
    }
    return render(request,'ppid/profil.html', context) 

# --------------------------------------------- END MENU PROFIL ----------------------------------------------------------
def opd(request):
    siteID = get_siteID(request)

    dinas = Dinas.objects.filter(site_id = siteID)

    site = Site.objects.filter(id = siteID)
    show = Data.objects.filter(title__icontains='profil', site_id = siteID)
    dip = Data.objects.filter(site_id = siteID)
    dip_count = dip.count()
    
    dip1_count = Data.objects.filter(type_data = '2', site_id = siteID).count()
    dip2_count = Data.objects.filter(type_data = '3', site_id = siteID).count()
    dip3_count = Data.objects.filter(type_data = '4', site_id = siteID).count()

    subQry = Subquery(Data.objects.filter(site_id = siteID, dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.filter(site_id = siteID).annotate(jumlah=subQry)

    # Subquery untuk menghitung jumlah data (Count), total view_count dan total download_count
    subQry = Data.objects.filter(
        site_id=siteID, dinas_id=OuterRef('id')
    ).values(
        'dinas_id'
    ).annotate(
        jml=Count('id'),
        total_views=Sum('view_count'),
        total_downloads=Sum('download_count')
    ).values(
        'jml', 'total_views', 'total_downloads'
    )

    # Annotate pada queryset Dinas untuk menambahkan field jumlah, total_views, dan total_downloads
    opd_count = Dinas.objects.filter(site_id=siteID).annotate(
        jumlah=Subquery(subQry.values('jml')),
        total_views=Subquery(subQry.values('total_views')),
        total_downloads=Subquery(subQry.values('total_downloads'))
    )

    total_view_count = Data.objects.filter(site_id=siteID).aggregate(total_views=Sum('view_count'))['total_views'] or 0
    total_download_count = Data.objects.filter(site_id=siteID).aggregate(total_downloads=Sum('download_count'))['total_downloads'] or 0

    p = Paginator(show, 10)

    page_num = request.GET.get('page',1)
    myMenu = menus.ClsMenus(siteID, False)

    try:
        show = p.page(page_num)
    except EmptyPage:
        show = p.page(1)

    context = {
        'dinas':dinas,
        'site':site,
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'total_view_count': total_view_count,
        'total_download_count': total_download_count,
        'menu': myMenu.get_menus(), 
        'activeMenuList': myMenu.find_activeMenuList('Opd')
    }
    return render(request, 'ppid/opd.html', context)


def error_404_view(request, exception):
    return render(request, 'ppid/404.html')

def sukses(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    context = {
        'menu': myMenu.get_menus(), 
    }
    return render(request, 'ppid/sukses.html', context)


# ---------------PROGRAM DAN KEGIATAN-------------------

def kegiatan(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(title__icontains='kegiatan', site_id = siteID)
    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/programkegiatan/kegiatan.html', context)

def agenda(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(title__icontains='agenda', site_id = siteID)
    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/programkegiatan/agenda.html', context)

def hakMasyarakat(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(Q(title__icontains='hak', site_id = siteID)|Q(title__icontains='masyarakat', site_id = siteID))
    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/programkegiatan/hakMasyarakat.html', context)

def laporanAkuntaKinerja(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(Q(title__icontains='laporan', site_id = siteID)|Q(title__icontains='kinerja', site_id = siteID)
    |Q(title__icontains='akuntabilitas', site_id = siteID))

    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/programkegiatan/laporanAkuntaKinerja.html', context)

def laporan(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(title__icontains='laporan', site_id = siteID)
    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/regulasi/laporan.html', context)

def sop(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(title__icontains='sop', site_id = siteID)
    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/regulasi/sop.html', context)

def regulasiKip(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(Q(title__icontains='regulasi', site_id = siteID)
    |Q(title__icontains='informasi dan dokumentasi', site_id = siteID)|Q(title__icontains='undang-undang', site_id = siteID)
    |Q(title__icontains='undang undang', site_id = siteID)
    |Q(title__icontains='pelayanan publik', site_id = siteID))

    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/regulasi/regulasi_kip.html', context)

def tataCaraPermohonan(request):
    siteID = get_siteID(request)
    myMenu = menus.ClsMenus(siteID, False)

    show = Data.objects.filter(Q(title__icontains='tata cara permohonan', site_id = siteID))

    dip = Data.objects.all()
    dip_count = dip.count()

    dip_1 = Data.objects.filter(type_data = '2')
    dip1_count = dip_1.count()

    dip_2 = Data.objects.filter(type_data = '3')
    dip2_count = dip_2.count()

    dip_3 = Data.objects.filter(type_data = '4')
    dip3_count = dip_3.count()

    subQry = Subquery(Data.objects.filter(dinas_id = OuterRef('id')).values('dinas_id').annotate(jml=Count('id')).values('jml'))
    opd_count = Dinas.objects.all().annotate(jumlah=subQry)

    context = {
        'show':show,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'opd_count':opd_count,
        'menu': myMenu.get_menus(), 

    }
    return render(request, 'ppid/regulasi/tata_cara_permohonan.html', context)