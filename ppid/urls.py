from django.urls import path, include

# from ppid import viewset_api
from . import views
# from ppid.viewset_api import *
# from rest_framework import routers

# router = routers.DefaultRouter()
# router.register('Data', DataViewset)

# urlpatterns = [
#     path('api/', include(router.urls)),

urlpatterns = [
    # ----------------------------------- API --------------------------------------------
    # path('api/', include(router.urls)),
    path('api/datas/', views.DataApiView.as_view(), name="datas"),
    path('api/permohonan/', views.PermohonanListApiView.as_view()),
    # path('api/example/', views.ExampleListView.as_view(), name='api-example'),
    # ----------------------------------- END API --------------------------------------------

    # ----------------------------------- URL FRONTEND --------------------------------------------
    path('', views.index, name="index"),
    path('DIP/', views.DIP, name="DIP"),
    path('profile-ppid/', views.profileppid, name="profileppid"), 
    path('dip-opd/<int:pk>', views.dip_opd, name="dip_opd"),
    path('dip-read/', views.dip_read, name="dip_read"),
    path('dip-download/', views.dip_download, name="dip_download"),
    path('laporanpermohonan/', views.jumlah_permohonan_layanan, name="laporanpermohonan"),
    path('contact/', views.contact, name="contact"),
    path('menu/<slug:slug>', views.halaman, name="halaman"),
    path('formpermohonan/', views.form_request, name="formrequest"),
    # ----------------------------------- END URL FRONTEND --------------------------------------------
    
    # ----------------------------------- URL READ & DOWNLOAD --------------------------------------------
    path('detail-data/<str:slug>', views.Detaildata, name="detail"),
    path('download-data/<str:slug>', views.Downloaddata, name="download"),
    # ----------------------------------- END URL READ & DOWNLOAD --------------------------------------------
    
    # ----------------------------------- SEARCH --------------------------------------------
    path('search/', views.search, name="search"),
    path('searchall/', views.searchall, name="searchall"),
    # ----------------------------------- END SEARCH --------------------------------------------

    # ----------------------------------- LAPORAN LAYANAN --------------------------------------------
    path('layananditerima/', views.layanan_diterima, name="layananditerima"),
    path('layananditolak/', views.layanan_ditolak, name="layananditolak"),

    path('layananwaktu/', views.layanan_waktu, name="layananwaktu"),
    path('layananpermohonan/', views.layanan_permohonan, name="layananpermohonan"),
    # ----------------------------------- LAPORAN LAYANAN --------------------------------------------

    path('opd/', views.opd, name="opd"),

    path('sukses/', views.sukses, name="sukses"),

    # --------------------------------------PROGRAM DAN KEGIATAN---------------------------------------
    path('kegiatan/', views.kegiatan, name="kegiatan"),
    path('agenda/', views.agenda, name="agenda"),
    path('hakmasyarakat/', views.hakMasyarakat, name="hakmasyarakat"),
    path('laporanakuntabilitaskinerja/', views.laporanAkuntaKinerja, name="laporanakuntabilitaskinerja"),

    # --------------------------------------REGULASI---------------------------------------
    path('laporan/', views.laporan, name="laporan"),
    path('sop/', views.sop, name="sop"),
    path('regulasikip/', views.regulasiKip, name="regulasikip"),
    path('tatacarapermohonan/', views.tataCaraPermohonan, name="tatacarapermohonan"),

    
    

]
