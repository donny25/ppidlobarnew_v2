from django.db import models
#from django.db.models.deletion import CASCADE, SET_NULL
#from django.db.models.fields import CharField, NullBooleanField
#from django.shortcuts import reverse
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.utils.text import slugify
from accounts.commonf import get_natural_datetime
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.dispatch import receiver
from django.template.defaultfilters import slugify
import itertools

import os

# Create your models here.

class Dinas(models.Model):
    class Meta:
        verbose_name = "Data Dinas"
        verbose_name_plural = "Data Dinas"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=225, null=True, blank=True)
    shortness = models.CharField(max_length=50, null=True, blank=True)
    website = models.CharField(max_length=255, null=True, blank=True)
    telp = models.CharField(max_length=15, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)

    list_display = ('title','shortness')

    def __str__(self):
        # return self.shortness
        return '{} - {}'.format(self.title, self.shortness)

class Type_data(models.Model):
    class Meta:
        verbose_name = "Tipe Data"
        verbose_name_plural = "Tipe Data"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)
    type = models.CharField(max_length=225, null=True, blank=True)                                          
    
    def __str__(self):
        return self.type

class IpModel(models.Model):
    ip = models.CharField(max_length=225, null=True, blank=True)

    def __str__(self):
        return self.ip

class Data(models.Model):
    class Meta:
        verbose_name = "Data PPID"
        verbose_name_plural = "Data PPID"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    code = models.CharField(max_length=125, blank=True, null=True)
    title = models.CharField(max_length=225, null=True, blank=True)
    slug = models.SlugField(max_length=255, default='', unique=True, blank=True, null=True)
    responsible = models.CharField(max_length=225, null=True, blank=True)
    dinas = models.ForeignKey(Dinas, on_delete=models.SET_NULL, null=True, blank=True) 
    information = models.CharField(max_length=225, null=True, blank=True)
    date = models.DateField(auto_now_add=True, null=True, blank=True)
    date_a = models.DateField(null=True, blank=True)
    date_b = models.DateField(null=True, blank=True)
    file = models.FileField()
    view_count = models.IntegerField(null=True, blank=True )
    download_count = models.IntegerField(null=True, blank=True)
    type_data = models.ForeignKey(Type_data, on_delete=models.SET_NULL, null=True, blank=True)
    size = models.CharField(max_length=225, null=True, blank=True)
    
    def __str__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        for i in itertools.count(1):
            if not Data.objects.filter(slug=self.slug).exclude(pk=self.pk).exists():
                break
            self.slug = '%s-%d' % (orig, i)
        super().save(*args, **kwargs)
    
    def delete(self, *args, **kwargs):
        self.file.delete()
        super().delete(*args, **kwargs)

    def total_reads(self):
        return self.read.count()

    def total_downloads(self):
        return self.download.count()
    
    def filesize(self):
        x = self.file.size
        y = 512000
        if x < y:
            value = round(x/1000, 2)
            ext = ' KB'
        elif x < y*1000:
            value = round(x/1000000, 2)
            ext = ' MB'
        else:
            value = round(x/1000000000, 2)
            ext = ' GB'
        return str(value)+ext
    
    def dinas_(self):
        if self.dinas is None:
            return ""
        return self.dinas.shortness

class Download_data(models.Model):
    class Meta:
        verbose_name = "Data Download"
        verbose_name_plural = "Data Download"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)
    data_file = models.ForeignKey(Data, null=True, blank=True, on_delete=models.SET_NULL)
    stats_download = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return self.data_file

class Type_pemohon(models.Model):
    class Meta:
        verbose_name = "Tipe Pemohon Data"
        verbose_name_plural = "Tipe Pemohon Data"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)
    type_pemohon = models.CharField(max_length=225, null=True, blank=True)
    
    def __str__(self):
        return self.type_pemohon

class Type_action(models.Model):
    class Meta:
        verbose_name = "Tipe Aksi"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)
    type_action = models.TextField(null=True, blank=True) 

    def __str__(self):
        return self.type_action
    
class Form_information(models.Model):
    class Meta:
        verbose_name = "Form Permintaan Data"
        verbose_name_plural = "Form Permintaan Data"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=255, null=True, blank=True)
    kategory_pemohon = models.ForeignKey(Type_pemohon, on_delete=models.SET_NULL, null=True, blank=True)
    action = models.ForeignKey(Type_action, on_delete=models.SET_NULL, null=True, blank=True)
    dinas = models.ForeignKey(Dinas, null=True, blank=True, on_delete=models.SET_NULL)
    address = models.TextField(null=True, blank=True)
    telp = models.CharField(max_length=25, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    ktp = models.FileField(null=True, blank=True)
    purpose = models.TextField(null=True, blank=True)
    detail = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    status_choice = (
        ('Belum Diproses','Belum Diproses'),
        ('Diberikan', 'Diberikan'),
        ('Ditolak', 'Ditolak'),
    )
    status = models.CharField(max_length=125, choices=status_choice, null=True, blank=True)
    Information = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.purpose
    
    def delete(self, *args, **kwargs):
        self.ktp.delete()
        super().delete(*args, **kwargs)
    
    def action_(self):
        if self.action is None:
            return ''
        return self.action.type_action
    
    def date_(self):
        return get_natural_datetime(self.date)
        

class sengketa(models.Model):
    class Meta:
        verbose_name = "Sengketa"
        verbose_name_plural = "Sengketa"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    name = models.CharField(max_length=225, null=True, blank=True) 
    address = models.TextField(null=True, blank=True) 
    profession = models.CharField(max_length=255, blank=True, null=True)
    telp = models.CharField(max_length=225,null=True, blank=True) 
    email = models.CharField(max_length=225, null=True, blank=True) 
    name_kuasa = models.CharField(max_length=225, null=True, blank=True) 
    address_kuasa = models.TextField(null=True, blank=True) 
    telp_kuasa = models.CharField(max_length=225, null=True, blank=True) 
    reason = models.TextField(null=True, blank=True) 
    date = models.DateTimeField(auto_now_add=True) 
    status = models.CharField(max_length=225, null=True, blank=True) 

    def __str__(self):
        return self.reason

class Type_layanan(models.Model):
    class Meta:
        verbose_name = "Tipe Layanan"
        verbose_name_plural = "Tipe Layanan"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.title

class layanan(models.Model):
    class Meta:
        verbose_name = "Layanan PPID"
        verbose_name_plural = "Layanan PPID"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    jenis = models.CharField(max_length=255, null=True, blank=True)
    type_layanan = models.ForeignKey(Type_layanan, null=True, blank=True, on_delete=models.PROTECT)
    file = models.FileField(null=True, blank=True)

    def __str__(self):
        return self.jenis
    
    def delete(self, *args, **kwargs):
        self.file.delete()
        super().delete(*args, **kwargs)


class slideshow(models.Model):
    class Meta:
        verbose_name = "SlideShow"
        verbose_name_plural = "SlideShow"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    description = models.TextField(null=True, blank=True)
    slide1 = models.ImageField(blank=True, null=True)
    slide2 = models.ImageField(blank=True, null=True)
    pelayanan = models.ImageField(blank=True, null=True)
    permohonan = models.ImageField(blank=True, null=True)  

    def __str__(self):
        return '{} - {}'.format(self.site, self.description)
    
    def image_url1(self):
        if self.slide1 and hasattr(self.slide1, 'url'):
            return self.slide1.url
    
    def image_url2(self):
        if self.slide2 and hasattr(self.slide2, 'url'):
            return self.slide2.url
    
    def image_url3(self):
        if self.pelayanan and hasattr(self.pelayanan, 'url'):
            return self.pelayanan.url
    
    def image_url4(self):
        if self.permohonan and hasattr(self.permohonan, 'url'):
            return self.permohonan.url
    
    # def save(self, *args, **kwargs):
    #     self.slide1
    #     super(slideshow, self).save(*args, **kwargs)
    
    def delete(self, *args, **kwargs):
        self.slide1.delete()
        self.slide2.delete()
        self.pelayanan.delete()
        self.permohonan.delete()
        super().delete(*args, **kwargs)
    
    # def delete(self, **kwargs):
    #     if self.slide1:
    #         if os.path.isfile(self.slide1.path):
    #             os.remove(self.slide1.path)

class profilppid(models.Model):
    class Meta:
        verbose_name = "Profil PPID"
        verbose_name_plural = "Profil PPID"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    description = RichTextField(null=True, blank=True)

    def __str__(self):
        return self.title

class tugasfungsi(models.Model):
    class Meta:
        verbose_name = "Tugas dan Fungsi PPID"
        verbose_name_plural = "Tugas dan Fungsi PPID"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    description = RichTextField(null=True, blank=True)
    perda = models.CharField(max_length=255)

    def __str__(self):
        return self.title
        
class visimisippid(models.Model):
    class Meta:
        verbose_name = "Visi dan Misi PPID"
        verbose_name_plural = "Visi dan Misi PPID"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    description = RichTextField(null=True, blank=True)

    def __str__(self):
        return self.title

class strukturorganisasi(models.Model):
    class Meta:
        verbose_name = "Struktur Organisasi PPID"
        verbose_name_plural = "Struktur Organisasi PPID"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    file = models.ImageField(blank=True)

    def __str__(self): 
        return self.title
    
    def delete(self, *args, **kwargs):
        self.file.delete()
        super().delete(*args, **kwargs)
    
    def image_url(self):
        if self.file and hasattr(self.file, 'url'):
            return self.file.url

class maklumatpelayanan(models.Model):
    class Meta:
        verbose_name = "Maklumat Pelayanan PPID"
        verbose_name_plural = "Maklumat Pelayanan PPID"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    file = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.title
    
    def delete(self, *args, **kwargs):
        self.file.delete()
        super().delete(*args, **kwargs)
    
    def image_url(self):
        if self.file and hasattr(self.file, 'url'):
            return self.file.url

class contact_person(models.Model):
    class Meta:
        verbose_name = "Contact Person"
        verbose_name_plural = "Contact Person"
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    street = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    telp = models.CharField(max_length=255, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    website = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.title


class menu(models.Model):
    class Meta:
        verbose_name = "Menu PPID"
        verbose_name_plural = "Menu PPID"
    site = models.ManyToManyField(Site)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=
        models.CASCADE)
    nama = models.CharField(max_length=250)
    href = models.CharField(max_length=255, null=True, blank=True,
        verbose_name='Link')
    icon = models.CharField(max_length=50, null=True, blank=True, default=0)
    slug = models.SlugField(max_length=250, null=True, blank=True, editable=False, unique=True)
    order_menu = models.IntegerField(default=7)
    is_admin_menu = models.BooleanField(default=False)
    is_visibled = models.BooleanField(default=True)
    is_master_menu = models.BooleanField(default=False)
    is_statis_menu = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.is_admin_menu:
            res = '[ Admin ]'
        else:
            res = '[ Front End ]'
        if self.parent:
            par = self.parent.nama
        else:
            par = ''
        return '{} {} > {}'.format(res, par, self.nama)
    
    def save(self, *args, **kwargs):
        self.slug = orig = slugify(self.nama)
        if not self.href:
            self.href = f'menu/{self.slug}'
        for i in itertools.count(1):
            if not menu.objects.filter(slug=self.slug).exclude(pk=self.pk).exists():
                break
            self.slug = '%s-%d' % (orig, i)
        super().save(*args, **kwargs)


class halaman_statis(models.Model):
    class Meta:
        verbose_name = "Halaman Statis PPID"
        verbose_name_plural = "Halaman Statis PPID"
    site = models.ForeignKey(Site,  null=True, blank=True, on_delete=models.CASCADE)
    judul = models.CharField(max_length=500)
    isi_halaman = RichTextUploadingField(blank=True, null=True)
    menu = models.ForeignKey(menu, on_delete=models.PROTECT)

    def __str__(self):
        return self.judul

    def get_absolute_url(self):
        return '/%s/%s' % ('halaman statis', self.judul)

class link_terkait(models.Model):
    class Meta:
        verbose_name = "Link Terkait"
        verbose_name_plural = "Link Terkait"
    site = models.ForeignKey(Site,  null=True, blank=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    href = models.CharField(max_length=255)
    image = models.ImageField(blank= True, null=True)
    width = models.CharField(max_length=255,blank=True, null=True)
    height = models.CharField(max_length=255,blank=True, null=True)

    def __str__(self):
        return self.title

@receiver(models.signals.post_delete, sender=slideshow)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    try:
        if instance.slide1:
            if os.path.isfile(instance.slide1.path):
                os.remove(instance.slide1.path)
    finally:
        return True

@receiver(models.signals.post_delete, sender=link_terkait)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    try:
        if instance.image:
            if os.path.isfile(instance.image.path):
                os.remove(instance.image.path)
    finally:
        return True

@receiver(models.signals.post_delete, sender=Form_information)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    try:
        if instance.ktp:
            if os.path.isfile(instance.ktp.path):
                os.remove(instance.ktp.path)
    finally:
        return True

@receiver(models.signals.pre_save, sender=slideshow)
def auto_delete_file_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False
    try:
        old_file = sender.objects.get(pk=instance.pk).slide1
    except sender.DoesNotExist:
        return False
    try:
        new_file = instance.slide1
        if not old_file == new_file:
            if old_file:
                if os.path.isfile(old_file.path):
                    os.remove(old_file.path)
    finally:
        return True

@receiver(models.signals.pre_save, sender=link_terkait)
def auto_delete_file_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False
    try:
        old_file = sender.objects.get(pk=instance.pk).image
    except sender.DoesNotExist:
        return False
    try:
        new_file = instance.image
        if not old_file == new_file:
            if old_file:
                if os.path.isfile(old_file.path):
                    os.remove(old_file.path)
    finally:
        return True

    
