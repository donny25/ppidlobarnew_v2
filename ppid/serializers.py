from django.db.models import fields
from ppid.models import Data, Form_information
from rest_framework import serializers

class DataSerializers(serializers.ModelSerializer):
    judul_dip = serializers.CharField(source="title")
    kode_ppid = serializers.CharField(source="code")
    kode = serializers.CharField(source="dinas_")
    penanggungjawab = serializers.CharField(source="responsible")
    keterangan = serializers.CharField(source="information")
    jenis_data = serializers.CharField(source="type_data")
    class Meta:
        model = Data
        fields = ['id','kode_ppid','judul_dip', 'kode', 'penanggungjawab', 'file', 'keterangan', 'jenis_data', 'date', 'date_a', 'date_b', 'view_count', 'download_count', 'size']

