# Generated by Django 4.0.1 on 2022-03-07 01:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ppid', '0029_remove_ipmodel_site_alter_form_information_ktp'),
    ]

    operations = [
        migrations.AlterField(
            model_name='form_information',
            name='ktp',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]
