# Generated by Django 4.0.1 on 2022-03-14 04:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
        ('ppid', '0033_visimisi_strukturorganisasi_maklumatpelayanan'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='visimisi',
            new_name='visimisippid',
        ),
    ]
