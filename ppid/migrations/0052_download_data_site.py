# Generated by Django 4.0.1 on 2022-04-22 04:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
        ('ppid', '0051_download_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='download_data',
            name='site',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='sites.site'),
        ),
    ]
