from .models import menu
from django.db.models import F
import logging
logger = logging.getLogger(__name__)


class ClsMenus:
    mDict = {}
    mList = []

    def __init__(self, key_filter=0, pIs_admin_menu=False, pIs_master_menu=
        False):
        if key_filter > 0:
            if len(self.mList) == 0:
                self.create_menus(key_filter, pIs_admin_menu, pIs_master_menu)
            else:
                self.mDict = {}
                self.mList = []
                self.create_menus(key_filter, pIs_admin_menu, pIs_master_menu)

    def get_menus(self):
        return self.mList

    def convert_StrToList(self, str):
        li = list(str.split(' - '))
        return li

    def find_activeMenuList(self, act_menu):
        mCount = 0
        mList = []
        while mCount < len(self.mList):
            if act_menu.lower() == self.mList[mCount]['nama'].lower():
                mList = self.mList[mCount]['activeMenu']
                break
            mCount += 1
        return mList

    def get_menuLevel(self, key_filter, menu_id):
        id = menu_id
        mlevel = 0
        mid = menu.objects.filter(site__id=key_filter, id=id).first()
        if mid:
            while mid.parent_id is not None:
                mlevel += 1
                id = mid.parent_id
                mid = menu.objects.filter(site__id=key_filter, id=id).first()
                if not mid:
                    break
        return mlevel

    def create_breadCrumb(self, act_menu):
        mDict = {}
        mList = []
        mListCount = 0
        mCount1 = 0
        mParentID = 0
        mNama = ''
        if len(self.mList) > 0:
            mNama = self.mList[0]['nama']
            if mNama.lower() != act_menu.lower():
                mDict[mListCount] = {'id': mListCount, 'nama': mNama,
                    'href': self.mList[0]['href']}
            else:
                mDict[mListCount] = {'id': mListCount, 'nama': mNama,
                    'href': '#'}
            mList.append(mDict[mListCount])
            mListCount += 1
            mOrder = 1
        if mNama.lower() != act_menu.lower():
            while mCount1 < len(self.mList):
                mNama = self.mList[mCount1]['nama']
                if mNama.lower() == act_menu.lower():
                    mParentID = self.mList[mCount1]['parent_id']
                    mDict[mListCount] = {'id': mListCount, 'nama': self.
                        mList[mCount1]['nama'], 'href': '#'}
                    mList.append(mDict[mListCount])
                    mListCount += 1
                    break
                mCount1 += 1
            mCount1 = 0
            while mCount1 < len(self.mList):
                if mParentID is None:
                    break
                elif mParentID == self.mList[mCount1]['id']:
                    mParentID = self.mList[mCount1]['parent_id']
                    mDict[mListCount] = {'id': mListCount, 'nama': self.
                        mList[mCount1]['nama'], 'href': self.mList[mCount1]
                        ['href']}
                    mList.insert(mOrder, mDict[mListCount])
                    mListCount += 1
                    mOrder += 1
                    mCount1 = 0
                mCount1 += 1
        return mList

    def create_menus(self, key_filter, pIs_admin_menu, pIs_master_menu):
        menu.objects.filter(id=F('parent_id')).update(parent_id=None)
        if pIs_master_menu:
            mData = menu.objects.filter(site__id=key_filter, is_admin_menu=
                pIs_admin_menu).order_by('parent_id', 'order_menu')
        else:
            mData = menu.objects.filter(site__id=key_filter, is_admin_menu=
                pIs_admin_menu, is_visibled=True).order_by('parent_id',
                'order_menu')
        mID = ''
        for m in mData:
            mID = m.id
            self.mDict[mID] = {'id': mID, 'nama': m.nama, 'href': m.href,
                'icon': m.icon, 'parent_id': m.parent_id, 'is_visibled': m.
                is_visibled, 'slug': m.slug}
            self.mList.append(self.mDict[mID])
        mCount1 = 0
        mCount2 = 0
        mOrder = 0
        while mCount1 < len(self.mList):
            mCount2 = mCount1 + 1
            mOrder = 0
            while mCount2 < len(self.mList):
                if self.mList[mCount1]['id'] == self.mList[mCount2]['parent_id'
                    ]:
                    mOrder += 1
                    self.mList.insert(mCount1 + mOrder, self.mList[mCount2])
                    self.mList.pop(mCount2 + 1)
                mCount2 += 1
            mCount1 += 1
        mCount1 = 0
        mLevel = 0
        mLevel_prev = 0
        mParentID = 0
        mIsHaveChild = 0
        while mCount1 < len(self.mList):
            mLevel = self.get_menuLevel(key_filter, self.mList[mCount1]['id'])
            self.mList[mCount1]['level'] = mLevel
            self.mList[mCount1]['haveChild'] = 0
            if mCount1 > 0:
                if self.mList[mCount1]['parent_id'] == self.mList[mCount1 - 1][
                    'id']:
                    self.mList[mCount1 - 1]['haveChild'] = 1
            self.mList[mCount1]['haveChildEndTag'] = 0
            selisih = mLevel - mLevel_prev
            if selisih < 0:
                self.mList[mCount1 - 1]['haveChildEndTag'] = -selisih
            mLevel_prev = mLevel
            mCount1 += 1
        mBreadCrumb = ''
        mCount1 = 0
        mCount2 = 0
        while mCount1 < len(self.mList):
            mParentID = self.mList[mCount1]['parent_id']
            mCount2 = 0
            mBreadCrumb = ''
            while mCount2 < len(self.mList):
                if mParentID is None:
                    if mBreadCrumb != '':
                        mBreadCrumb += ' - '
                    mBreadCrumb += self.mList[mCount1]['nama']
                    break
                elif mParentID == self.mList[mCount2]['id']:
                    if mBreadCrumb != '':
                        mBreadCrumb += ' - '
                    mBreadCrumb += self.mList[mCount2]['nama']
                    mParentID = self.mList[mCount2]['parent_id']
                    mCount2 = 0
                mCount2 += 1
            self.mList[mCount1]['activeMenu'] = self.convert_StrToList(
                mBreadCrumb.lower())
            mCount1 += 1
