from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView, ListCreateAPIView, UpdateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics

# from django_filters.rest_framework import DjangoFilterBackend

from django.db.models import Sum, F
from ppid.models import Data, Dinas

from ppid.api.serializers import (DataSerializer, DataDetailSerializer, DataOpdSerializer, DataOpdDetailSerializer)
from django.contrib.auth.models import User


class DataListView(ListAPIView):
    serializer_class = DataSerializer
    queryset = Data.objects.all().order_by("date")
    search_fields = ['title']
    ordering_fields = ['title']

    filter_backends = (
        SearchFilter,
        OrderingFilter
    )

class DataDetailView(RetrieveAPIView):
    serializer_class = DataDetailSerializer
    queryset = Data.objects.all()

class DinasListView(ListAPIView):
    serializer_class = DataOpdSerializer
    queryset = Dinas.objects.all()
    search_fields = ['title']
    ordering_fields = ['title']

    filter_backends = (
        SearchFilter,
        OrderingFilter
    )

class DinasDetailListView(RetrieveAPIView):
    serializer_class = DataOpdDetailSerializer
    queryset = Dinas.objects.all()

class DataByDinasAPIView(generics.ListAPIView):
    serializer_class = DataSerializer

    def get_queryset(self):
        dinas_id = self.kwargs['dinas_id']
        return Data.objects.filter(dinas=dinas_id)
  