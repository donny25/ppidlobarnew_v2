from rest_framework import serializers

from ppid.models import Data, Dinas
from django.contrib.auth.models import User

class DataSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source="type_data")
    dinas = serializers.CharField(source="dinas_")
    class Meta:
        model = Data
        fields = ('id', 'title', 'code', 'dinas', 'type', 'size', 'file', 'responsible', 'view_count', 'download_count','slug')

class DataDetailSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source="type_data")
    dinas = serializers.CharField(source="dinas_")
    class Meta:
        model = Data
        fields = ('id', 'title', 'code', 'responsible', 'dinas', 'information', 'date', 'file', 'type', 'size', 'view_count', 'download_count')

class DataOpdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dinas
        fields = ('id', 'title','shortness')

class DataOpdDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dinas
        fields = ('id', 'title','shortness', 'website', 'telp', 'email')

