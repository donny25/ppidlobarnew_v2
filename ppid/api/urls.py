from django.urls import path

from .views import (DataListView, DataDetailView, DinasListView, DinasDetailListView, DataByDinasAPIView)

# from rest_framework_simplejwt.views import TokenObtainPairView

urlpatterns = [
    
    # Api Data
    path('data/', DataListView.as_view(), name='api-data'),
    path('data/<int:pk>', DataDetailView.as_view(), name='api-data-detail'),

    #Api Dinas
    path('dinas/', DinasListView.as_view(), name='api-dinas'),
    path('dinas/<int:pk>', DinasDetailListView.as_view(), name='api-dinas-detail'),

    path('data/dinas/<int:dinas_id>/', DataByDinasAPIView.as_view(), name='data-by-dinas'),

]