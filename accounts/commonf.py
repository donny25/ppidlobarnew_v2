from django.contrib.humanize.templatetags.humanize import (naturalday,
                                                           naturaltime)
from django.http import HttpResponse

import calendar, datetime

def get_natural_datetime(data_datetime):
    tmp = naturalday(data_datetime)
    if tmp == 'hari ini':
        return naturaltime(data_datetime)    
    return tmp    

def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = max(sourcedate.day, calendar.monthrange(year,month)[1])
    return datetime.date(year, month, day)

