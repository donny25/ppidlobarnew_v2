from django.urls import path, include
from . import views
from .forms import MyPasswordChangeForm
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('logout/', views.logoutPage, name="logout"),
    path('accounts/login/', views.loginPage, name='login'),
    path('accounts/', views.loginPage, name='login'),
    path('ppid/', views.loginPage, name='login'),
    path('login/', views.loginPage, name="login"),
    path('admin/', views.loginPage, name="login"),

    path('register/', views.registerPage, name="register"),
    path('dashboard/', views.dashboard, name="dashboard"),
    path('data-ppid/', views.data_ppid, name="datappid"),
    path('data-ppid-tahunan/', views.data_ppid_tahunan, name="datappidtahunan"),
    path('profile-admin/<str:pk>', views.profile, name="profile"),

    path('create-data/', views.create_data, name="create"),
    path('update-data/<int:pk>', views.update_data, name="update"),
    path('delete-data/<int:pk>', views.delete_data, name="delete"),

    path('permohonan/', views.permohonan_data, name="permohonan"),
    path('proses-permohonan/<int:pk>', views.proses_permohonan, name="prosespermohonan"),
    path('delete-permohonan/<int:pk>', views.delete_permohonan, name="deletepermohonan"),
    
    path('change-password/', views.MyPasswordChangeView.as_view(form_class=MyPasswordChangeForm), name="change-password-view"),

    path('menu/', views.daftarMenu, name="menu"),
    path('create-menu/', views.createMenu, name="create_menu"),
    path('delete-menu/<str:pk>', views.deleteMenu, name="delete_menu"),
    path('update-menu/<str:pk>', views.updateMenu, name="update_menu"),

    path('halaman-statis/', views.halamanStatis, name="halaman_statis"),
    path('create-halaman-statis/', views.createHalamanStatis, name="create_halaman_statis"),
    path('delete-halaman-statis/<int:pk>', views.deleteHalamanStatis, name="delete_halaman_statis"),
    path('update-halaman-statis/<int:pk>', views.updateHalamanStatis, name="update_halaman_statis"),

    path('profil-ppid/', views.profilPPID, name="profil-ppid"),
    path('visimisi-ppid/', views.visimisiPPID, name="visimisi-ppid"),
    path('tugasfungsi-ppid/', views.tugasfungsiPPID, name="tugasfungsi-ppid"),
    path('strukturorganisasi-ppid/', views.strukturorganisasiPPID, name="strukturorganisasi-ppid"),
    path('maklumatpelayanan-ppid/', views.maklumatpelayananPPID, name="maklumatpelayanan-ppid"),


    #     path('dashboard/', v.index, name='index'),
    # path('', v.indexuser, name='indexuser'),
    # path('register/', vr.register, name='register' ),
    # path('account/', include('django.contrib.auth.urls')),

    # path('login/', vr.login_redirect, name='login'),
    # path('account/', vr.login_redirect, name='login'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
