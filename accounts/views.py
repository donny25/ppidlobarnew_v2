import django
from ppid.models import Dinas, Form_information
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.core.paginator import EmptyPage, Paginator
from django.shortcuts import get_object_or_404

from django.db.models import Count
from django.db.models.functions import ExtractYear, ExtractMonth

# membuat akses login dan logout
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from ppid import menus

from django.urls import reverse_lazy

#reset password
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView

from .models import *
from .forms import *
from ppid import forms 
import logging
import os
logger = logging.getLogger(__name__)
from django.db.models import Q
from datetime import datetime
from django.db.models import Sum

# Create your views here.
def get_siteID(request):
    siteID = Site.objects.filter(domain=request.get_host()).values_list('id',
        flat=True)
    if siteID.count() == 0:
        return HttpResponse(
            "domain '%s' belum terdaftar, silahkan daftar di halaman <a href='%s'>admin</a>"
             % (request.get_host(), '/admin'))
    siteID = siteID[0]
    return siteID

class MyPasswordChangeView(PasswordChangeView):
    template_name = "account/password-change.html"
    success_url = reverse_lazy('dashboard')
    
    def get_context_data(self, **kwargs):
        show = models.Dinas.objects.filter(user_id = self.request.user.id)
        context = super().get_context_data(**kwargs)
        context['show'] = show
        return context


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('dashboard')
            else:
                messages.info(request, "Username atau Password Salah")
    
    context = {

    }
    return render(request, 'registration/login.html', context)

def user_is_active(view_func):
    def wrap(request, *args, **kwargs):
        if not request.user.is_superuser and not request.user.is_staff:
            return render(request, 'account/404.html')
        return view_func(request, *args, **kwargs)
    return wrap


@login_required(login_url="/accounts/login/")
def logoutPage(request):
    logout(request)
    return redirect('logout')

@login_required(login_url="/accounts/login/")
def registerPage(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()

    context = { 
        'form':form
    }
    return render(request, 'registration/register.html', context)

MONTH_NAMES = {
    1: 'Januari',
    2: 'Februari',
    3: 'Maret',
    4: 'April',
    5: 'Mei',
    6: 'Juni',
    7: 'Juli',
    8: 'Agustus',
    9: 'September',
    10: 'Oktober',
    11: 'November',
    12: 'Desember'
}

@login_required(login_url="/accounts/login/")
def dashboard(request):
    siteID = get_siteID(request)
    show = models.Dinas.objects.filter(site_id = siteID, user_id = request.user.id)
    
    qry = models.Dinas.objects.filter(user_id = request.user.id, site_id = siteID).values_list("id", flat=True)

    qry1 = models.Dinas.objects.filter(user_id = request.user.id, site_id = siteID).values_list("title")
    qry_namadinas = qry1[0]
    
    logger.error(qry)
    if qry.count() == 0:
        return HttpResponse(
            "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
             % (request.user.username, request.get_host(), '/logout'))

    # context['instansi'] = instansi.objects.filter(site_id=siteID)[:1]

    dip = models.Data.objects.filter(dinas_id=qry[0])
    dipall = models.Data.objects.all()
    
    if request.user.is_superuser:
        dip_1 = models.Data.objects.filter(type_data='2')
        dip_2 = models.Data.objects.filter(type_data='3')
        dip_3 = models.Data.objects.filter(type_data='4')
        dip_count = dipall.count()
    else:
        dip_1 = models.Data.objects.filter(type_data='2', dinas_id=qry[0])
        dip_2 = models.Data.objects.filter(type_data='3', dinas_id=qry[0])
        dip_3 = models.Data.objects.filter(type_data='4', dinas_id=qry[0])
        dip_count = dip.count()
    
    dip1_count = dip_1.count()
    dip2_count = dip_2.count()
    dip3_count = dip_3.count()

    # Menghitung total view_count dan download_count
    total_view_count = dip.aggregate(Sum('view_count'))['view_count__sum'] or 0
    total_download_count = dip.aggregate(Sum('download_count'))['download_count__sum'] or 0
    
    if request.user.is_superuser:
        # Menghitung jumlah data berdasarkan tahun dan bulan
        dip_count_by_year_month = dipall.annotate(
            year=ExtractYear('date'),
            month=ExtractMonth('date')
        ).values('year', 'month').annotate(count=Count('id')).order_by('-year') 
    else:
         # Menghitung jumlah data berdasarkan tahun dan bulan
        dip_count_by_year_month = dip.annotate(
            year=ExtractYear('date'),
            month=ExtractMonth('date')
        ).values('year', 'month').annotate(count=Count('id')).order_by('-year') 

    context = {
        'show':show,
        'nama_dinas':qry_namadinas,
        'dip_count':dip_count,
        'dip3_count':dip3_count,
        'dip2_count':dip2_count,
        'dip1_count':dip1_count,
        'total_view_count': total_view_count,
        'total_download_count': total_download_count,
        'dip_count_by_year_month': [
            {'year': data['year'], 'month': MONTH_NAMES[data['month']], 'count': data['count']}
            for data in dip_count_by_year_month
        ]
    }
    return render(request, 'account/index.html', context)
    
@login_required(login_url="/accounts/login/")
def data_ppid(request):
    siteID = get_siteID(request)

    show = models.Dinas.objects.filter(site_id = siteID, user_id = request.user.id)

    #query set mengambil id dinas
    qry = models.Dinas.objects.filter(user_id = request.user.id, site_id = siteID).values_list("id", flat=True)
    # dinas_id = qry[0]

    logger.error(qry)
    if qry.count() == 0:
        return HttpResponse(
            "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
             % (request.user.username, request.get_host(), '/logout'))

    data = models.Data.objects.filter(dinas_id=qry[0]).order_by('-id')

    context = {
        'data':data,
        'show':show,
    }
    return render(request, 'account/data_ppid.html', context)

@login_required(login_url="/accounts/login/")
def data_ppid_tahunan(request):
    siteID = get_siteID(request)

    show = models.Dinas.objects.filter(site_id=siteID)

    #query set mengambil id dinas
    qry = models.Dinas.objects.all()

    logger.error(qry)
    if qry.count() == 0:
        return HttpResponse(
            "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
            % (request.user.username, request.get_host(), '/logout'))

    # Filter data untuk tahun 2024 menggunakan field 'date'
    data = models.Data.objects.filter(
        date__year=2024  # Menggunakan field 'date'
    ).order_by('-id')

    context = {
        'data': data,
        'show': show,
    }
    return render(request, 'account/data_ppid_tahunan.html', context)


@login_required(login_url="/accounts/login/")
def create_data(request):
    siteID = get_siteID(request)
    #query set mengambil id dinas
    qry = models.Dinas.objects.filter(user_id = request.user.id, site_id = siteID).values_list("id", flat=True)

    show = models.Dinas.objects.filter(site_id = siteID, user_id = request.user.id)
    codePde = models.Data.objects.filter(site_id = siteID, dinas_id=qry[0])

    form = DataForm()
    kodelist = models.Data.objects.filter(site_id = siteID, dinas_id=qry[0])

    if request.method == 'POST':
        form = DataForm(request.POST, request.FILES)
        if form.is_valid():
            obj = form.save(commit=False)
            
            qry = models.Dinas.objects.filter(user_id = request.user.id, site_id = siteID).values_list("id", flat=True)
            logger.error(qry)
            if qry.count() == 0:
                return HttpResponse(
                    "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
                    % (request.user.username, request.get_host(), '/logout'))

            obj.dinas_id = qry[0]
            obj.user_id = request.user.id
            obj.site_id = siteID
            # temp_file_obj = TemporaryFileUploadHandler(form.cleaned_data['file_path'])
            # form.instance.file_size = temp_file_obj.chunk_size

            obj.size = request.FILES['file'].size

            obj.save()
            
            return redirect('datappid')

    context = {
        'form':form,
        'show':show,
        'codePde':codePde,
        'kodelist':kodelist
    }
    return render(request, "account/create_data.html", context)

@login_required(login_url="/accounts/login/")
def update_data(request, pk):
    siteID = get_siteID(request)
    kodelist = models.Data.objects.all()
    show = models.Dinas.objects.filter(site_id = siteID, user_id = request.user.id)

    data = models.Data.objects.get(id=pk,  site_id = siteID)
    form = DataFormUpdate(instance = data)

    if request.method == 'POST':
        form = DataFormUpdate(request.POST, request.FILES, instance = data)
        if len(request.FILES) != 0:
            if len(data.file) > 0:
                os.remove(data.file.path)
                if form.is_valid():
                    obj = form.save(commit=False)
                    
                    qry = models.Dinas.objects.filter(user_id = request.user.id, site_id = siteID).values_list("id", flat=True)

                    logger.error(qry)
                    if qry.count() == 0:
                        return HttpResponse(
                            "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
                            % (request.user.username, request.get_host(), '/logout'))


                    obj.dinas_id = qry[0]
                    obj.site_id = siteID
                    obj.user_id = request.user.id
                    obj.size = request.FILES['file'].size

                    obj.save()
                    return redirect('datappid')
                    
        if form.is_valid():
            obj = form.save(commit=False)
            qry = models.Dinas.objects.filter(user_id = request.user.id, site_id = siteID).values_list("id", flat=True)
            logger.error(qry)
            if qry.count() == 0:
                return HttpResponse(
                    "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
                    % (request.user.username, request.get_host(), '/logout'))

            obj.dinas_id = qry[0]
            obj.site_id = siteID

            obj.user_id = request.user.id
            obj.save()
            return redirect('datappid')
    context = {
        'form':form,
        'show':show,
        'kodelist':kodelist
    }
    return render(request, 'account/edit_data.html', context)

@login_required(login_url="/accounts/login/")
def delete_data(request, pk):
    siteID = get_siteID(request)
    if request.method == "POST":
        data = models.Data.objects.get(id=pk, site_id = siteID)
        data.delete()
        messages.success(request, 'Data Berhasil Dihapus...')
    return redirect('datappid')


@login_required(login_url="/accounts/login/")
def profile(request, pk):
    siteID = get_siteID(request)

    show = models.Dinas.objects.filter(site_id = siteID, user_id = request.user.id)

    data = models.Dinas.objects.get(shortness=pk, site_id = siteID, user_id = request.user.id)
    form = ProfileForm(instance = data)
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance = data)
        if form.is_valid():
            obj = form.save(commit=False)
            
            qry = models.Dinas.objects.filter(user_id = request.user.id).values_list("id", flat=True)

            obj.dinas_id = qry[0]
            obj.site_id = siteID

            obj.user_id = request.user.id
            obj.save()

    context = {
        'form':form,
        'show':show
    }

    return render(request, 'account/profile.html', context)

@login_required(login_url="/accounts/login/")
def permohonan_data(request):
    siteID = get_siteID(request)

    #query set mengambil id dinas
    show = models.Dinas.objects.filter(site_id = siteID, user_id = request.user.id)
    
    qry = models.Dinas.objects.filter(user_id = request.user.id).values_list("id", flat=True)
    logger.error(qry)
    if qry.count() == 0:
        return HttpResponse(
            "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
            % (request.user.username, request.get_host(), '/logout'))
    
    # show = Data.objects.filter(site_id = '1').order_by("-date")[:7]

    data = models.Form_information.objects.filter(dinas_id = qry[0]).order_by("-date")

    context = {
        'data':data,
        'show':show,
    }
    return render(request, 'account/permohonan.html', context)

@login_required(login_url="/accounts/login/")
def proses_permohonan(request,pk):
    siteID = get_siteID(request)
    qry = models.Dinas.objects.filter(user_id = request.user.id).values_list("id", flat=True)
    show1 = models.Form_information.objects.filter(site_id = siteID, dinas_id = qry[0], id=pk)
    show = models.Dinas.objects.filter(site_id = siteID, user_id = request.user.id)
    permohonan = models.Form_information.objects.get(id=pk, site_id = siteID)
    form = RequestForm(instance = permohonan)
    if request.method == 'POST':
        form = RequestForm(request.POST, instance = permohonan)
        if form.is_valid():
            obj = form.save(commit=False)
            
            qry = models.Dinas.objects.filter(user_id = request.user.id).values_list("id", flat=True)
            logger.error(qry)
            if qry.count() == 0:
                return HttpResponse(
                    "username '%s' tidak di temukan di domain '%s'. <a href='%s'>Logout</a>"
                    % (request.user.username, request.get_host(), '/logout'))
            obj.kategory_pemohon = obj.kategory_pemohon 
            obj.dinas_id = qry[0]
            obj.site_id = siteID

            obj.user_id = request.user.id
            obj.save()
            return redirect('permohonan')
    context = {
        'show':show,
        'show1':show1,
        'form':form,
    }
    return render(request, 'account/detail_permohonan.html', context)

@login_required(login_url="/accounts/login/")
def delete_permohonan(request, pk):
    if request.method == "POST":
        data = models.Form_information.objects.get(id=pk)
        data.delete()
        messages.success(request, 'Data Berhasil Dihapus...')
    return redirect('permohonan')

# --------------------------------- HANYA USER AKTIF YANG BISA MENGAKSES MENU INI ------------------------------------------------------------------
@user_is_active
@login_required(login_url="/accounts/login/")
def daftarMenu(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)
    showmenu = models.menu.objects.filter(site__id = siteID).order_by("-id")
    context = {
         'menu': myMenu.get_menus(), 
         'showmenu' : showmenu,
    }

    return render(request, 'account/menu.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def createMenu(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)

    form = MenuForm()

    if request.method == 'POST':
        form = MenuForm(request.POST)
        if form.is_valid():
            obj = form.save()
            obj.site.set(form.cleaned_data['site'])
            obj.save()
            messages.success(request, 'Menu Berhasil Ditambahkan...')
            
            return redirect('menu')

    context = {
         'menu': myMenu.get_menus(), 
         'form':form,
    }

    return render(request, 'account/create_menu.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def updateMenu(request, pk):
    siteID = get_siteID(request)
    #Menu
    myMenu = menus.ClsMenus(siteID, False)

    menuGet = models.menu.objects.get(slug=pk)

    form = MenuForm(instance=menuGet)

    if request.method == 'POST':
        form = MenuForm(request.POST, instance=menuGet)
        if form.is_valid():
            obj = form.save()
            obj.site.set(form.cleaned_data['site'])
            obj.save()
            messages.success(request, 'Menu Berhasil Diubah...')
            
            return redirect('menu')

    context = {
         'menu': myMenu.get_menus(), 
         'form':form,
    }

    return render(request, 'account/create_menu.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def deleteMenu(request, pk):
    data = get_object_or_404(models.menu, slug=pk)
    if request.method == "POST":
        try:
            data.delete()
            messages.success(request, 'Menu Berhasil Dihapus...')
        except Exception as e:
            messages.error(request, f'Gagal menghapus menu:  Hapus Halaman Statis yang berelasi dengan Menu terlebih dahulu')
    return redirect('menu')
    
@user_is_active
@login_required(login_url="/accounts/login/")
def halamanStatis(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)
    showmenu = models.menu.objects.filter(site__id = siteID)
    showhalaman = models.halaman_statis.objects.filter(site_id = siteID)

    context = {
         'menu': myMenu.get_menus(), 
         'showmenu' : showmenu,
         'showhalaman':showhalaman,
    }

    return render(request, 'account/halamanStatis.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def createHalamanStatis(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)

    form = HalamanStatisForm()

    if request.method == 'POST':
        form = HalamanStatisForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.site_id = siteID
            obj.save()
            messages.success(request, 'Halaman Statis Berhasil Ditambahkan...')
            
            return redirect('halaman_statis')

    context = {
         'menu': myMenu.get_menus(), 
         'form':form,
    }

    return render(request, 'account/create_halaman_statis.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def updateHalamanStatis(request, pk):
    siteID = get_siteID(request)

    halamanGet = models.halaman_statis.objects.get(id=pk)
    #Menu
    myMenu = menus.ClsMenus(siteID, False)

    form = HalamanStatisForm(instance=halamanGet)

    if request.method == 'POST':
        form = HalamanStatisForm(request.POST, instance=halamanGet)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.site_id = siteID
            obj.save()
            messages.success(request, 'Halaman Statis Berhasil DiUbah...')
            
            return redirect('halaman_statis')

    context = {
         'menu': myMenu.get_menus(), 
         'form':form,
    }

    return render(request, 'account/create_halaman_statis.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def deleteHalamanStatis(request, pk):
    if request.method == "POST":
        data = models.halaman_statis.objects.get(id=pk)
        data.delete()
        messages.success(request, 'Halaman Statis Berhasil Dihapus...')
    return redirect('halaman_statis')

@user_is_active
@login_required(login_url="/accounts/login/")
def profilPPID(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)
    showProfil = models.profilppid.objects.filter(site_id = siteID)

    context = {
         'menu': myMenu.get_menus(), 
         'showProfil' : showProfil,
    }

    return render(request, 'account/ProfilPPID/profil.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def visimisiPPID(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)
    showProfil = models.visimisippid.objects.filter(site_id = siteID)

    context = {
         'menu': myMenu.get_menus(), 
         'showProfil' : showProfil,
    }

    return render(request, 'account/ProfilPPID/visimisippid.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def tugasfungsiPPID(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)
    showProfil = models.tugasfungsi.objects.filter(site_id = siteID)

    context = {
         'menu': myMenu.get_menus(), 
         'showProfil' : showProfil,
    }

    return render(request, 'account/ProfilPPID/tugasdanfungsi.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def strukturorganisasiPPID(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)
    showProfil = models.strukturorganisasi.objects.filter(site_id = siteID)

    context = {
         'menu': myMenu.get_menus(), 
         'showProfil' : showProfil,
    }

    return render(request, 'account/ProfilPPID/strukturorganisasi.html', context)

@user_is_active
@login_required(login_url="/accounts/login/")
def maklumatpelayananPPID(request):
    siteID = get_siteID(request)

    #Menu
    myMenu = menus.ClsMenus(siteID, False)
    showProfil = models.maklumatpelayanan.objects.filter(site_id = siteID)

    context = {
         'menu': myMenu.get_menus(), 
         'showProfil' : showProfil,
    }

    return render(request, 'account/ProfilPPID/maklumatpelayanan.html', context)



