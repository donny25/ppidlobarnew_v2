from django.db.models import fields
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import User
from django import forms
from ppid import models 
import mimetypes
from django.core.exceptions import ValidationError
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.utils.text import slugify

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class MyPasswordChangeForm(PasswordChangeForm):
	def __init__(self,  *args, **kwargs):
		super().__init__(*args, **kwargs)
		for fieldname in ['old_password', 'new_password1','new_password2']:
			self.fields[fieldname].widget.attrs = {'class':'form-control'}

class DataForm(ModelForm):
	
	class Meta:
		model = models.Data
		fields = '__all__'
		widgets = {
			'code': forms.TextInput(
				attrs = {
				'placeholder': 'Contoh DISHUB-001',
				'class':'form-control',
				'required':'',
				'list':'kodelist',
				'type':'text'
				}
			), 
			'responsible': forms.TextInput(
				attrs = {
				'placeholder': 'Penanggung Jawab Penerbitan',
				'class':'form-control',
				'required':''
				}
			), 
            'title': forms.TextInput(
				attrs = {
				'placeholder': 'Judul DIP',
				'class':'form-control',
				'required':''
				}
			), 
            'information': forms.Textarea(
				attrs = {
				'placeholder': 'Deskripsi',
				'class':'form-control',
				}
			), 
			'type_data': forms.Select(
				attrs = {
				'class':'form-control',
				'required':''
				}
			), 
            'file': forms.FileInput(
				attrs = {
				'class': 'form-control',
				'required':'',
				'accept':'.pdf, .doc, .docx, .xls, .xlsx,',
				}
			), 
            'date_a': forms.TextInput(
				attrs = {
				'type': 'date',
				'class':'form-control'
				}
			), 
            'date_b': forms.TextInput(
				attrs = {
				'type': 'date',
				'class':'form-control'
				}
			), 
		}

	def clean_code(self):
		code = self.cleaned_data.get('code')
		if (code == ""):
			raise forms.ValidationError('Kode Tidak Boleh Kosong')
		for instance in models.Data.objects.all():
			if instance.code == code:
				raise forms.ValidationError('Kode Sudah Pernah Digunakan')
		return code
		
	def clean_file(self):
		file = self.cleaned_data['file']
		file_type = mimetypes.guess_type(file.name)[0]
		if file_type not in ('application/pdf', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'):
			raise ValidationError('Type File Tidak Didukung. Hanya File pdf, doc, docx, xls, xlsx')
		return file


class DataFormUpdate(ModelForm):
	class Meta:
		model = models.Data
		fields = '__all__'
		widgets = {
			'code': forms.TextInput(
				attrs = {
				'placeholder': 'Contoh DISHUB-001',
				'class':'form-control',
				'required':'',
				'list':'kodelist',
				'type':'text'
				}
			), 
			'responsible': forms.TextInput(
				attrs = {
				'placeholder': 'Penanggung Jawab Penerbitan',
				'class':'form-control'
				}
			), 
            'title': forms.TextInput(
				attrs = {
				'placeholder': 'Judul DIP',
				'class':'form-control'
				}
			), 
            'information': forms.Textarea(
				attrs = {
				'placeholder': 'Deskripsi',
				'class':'form-control'
				}
			), 
			'type_data': forms.Select(
				attrs = {
				'class':'form-control'
				}
			), 
            'file': forms.FileInput(
				attrs = {
				'class': 'form-control',
				'accept':'.pdf, .doc, .docx, .xls, .xlsx,',
				}
			), 
            'date_a': forms.TextInput(
				attrs = {
				'type': 'date',
				'class':'form-control'
				}
			), 
            'date_b': forms.TextInput(
				attrs = {
				'type': 'date',
				'class':'form-control'
				}
			), 
			 'size': forms.TextInput(
				attrs = {
				'type': 'hidden',
				'class':'form-control'
				}
			), 
		}

	# def clean_file(self):
	# 	file = self.cleaned_data['file']
	# 	file_type = mimetypes.guess_type(file.name)[0]
	# 	if file_type not in ('application/pdf', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 
	# 	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'):
	# 		raise ValidationError('Type File Tidak Didukung. Hanya File pdf, doc, docx, xls, xlsx')
	# 	return file
		
		

class RequestForm(ModelForm):
    class Meta:
        model = models.Form_information
        fields = '__all__'
        widgets = {
			'name': forms.TextInput(
				attrs = {
				'placeholder': 'Nama Lengkap',
                'class' : 'form-control',
				'readonly' : ''
				}
			), 
            'address': forms.Textarea(
				attrs = {
				'class': 'form-control',
				'placeholder': 'Alamat',
				'readonly' : ''
				}
			), 
            'telp': forms.TextInput(
				attrs = {
				'placeholder': 'No Telp',
				'type': 'number',
                'class' : 'form-control',
				'readonly' : ''
				}
			), 
            'email': forms.TextInput(
				attrs = {
				'placeholder': 'Email',
                'class' : 'form-control',
				'readonly' : ''
				}
			), 
            'purpose': forms.Textarea(
				attrs = {
				'placeholder': 'Tujuan Penggunaan Informasi',
				'class' : 'form-control',
				'readonly' : ''
				}
			), 
             'detail': forms.Textarea(
				attrs = {
				'placeholder': 'Detail Informasi Yang Dibutuhkan',
				'class' : 'form-control',
				'readonly' : ''
				}
			), 
            'status': forms.Select(
				attrs = {
                'class':'form-control'
				}
			), 
            'dinas': forms.Select(
				attrs = {
				'readonly' : ''
				}
			), 
			'Information': forms.Textarea(
				attrs = {
				'class' : 'form-control'
				}
			), 
			'kategory_pemohon': forms.TextInput(
				attrs = {
				'class' : 'form-control',
				'type' : 'hidden'
				}
			), 
			'action': forms.TextInput(
				attrs = {
				'class' : 'form-control',
				'type' : 'hidden'
				}
			), 
			
		}

class ProfileForm(ModelForm):
    class Meta:
        model = models.Dinas
        fields = '__all__'
        widgets = {
			'title': forms.TextInput(
				attrs = {
                'class' : 'form-control',
				}
			), 
			'shortness': forms.TextInput(
				attrs = {
                'class' : 'form-control',
                'type' : 'hidden',
				}
			), 
			'email': forms.TextInput(
				attrs = {
                'class' : 'form-control',
				}
			), 
			'telp': forms.TextInput(
				attrs = {
                'class' : 'form-control',
				}
			), 
			'website': forms.TextInput(
				attrs = {
                'class' : 'form-control',
				}
			), 
		}


class MenuForm(ModelForm):
	class Meta:
		model = models.menu
		fields = ['parent', 'nama', 'href', 'icon', 'order_menu', 'site', 'is_visibled']
		widgets = {
			'parent': forms.Select(
				attrs = {
                'class':'form-control'
				}
			),
			'nama': forms.TextInput(
				attrs= {
					'class':'form-control',
					'placeholder':'Nama Menu',
				}
			),
			'icon': forms.TextInput(
				attrs= {
					'class':'form-control',
				}
			),
			'order_menu': forms.TextInput(
				attrs= {
					'class':'form-control',
					'type':'number',
				}
			),
			'site': forms.SelectMultiple(
				attrs= {
					'class':'form-control',
				}
			),
			'is_visibled': forms.CheckboxInput(
				attrs= {
					'class':'custom-control-input',
					'type':'checkbox',
					'id':'check-1',
				}
			),
		}

class HalamanStatisForm(ModelForm):
	isi_halaman = forms.CharField(widget=CKEditorUploadingWidget())
	class Meta:
		model = models.halaman_statis
		fields = ['judul', 'isi_halaman', 'menu']
		widgets = {
			'judul': forms.TextInput(
				attrs= {
					'class':'form-control',
					'placeholder':'Judl Pada Halaman',
				}
			),
			'menu': forms.Select(
				attrs= {
					'class':'form-control',
				}
			),
		}



